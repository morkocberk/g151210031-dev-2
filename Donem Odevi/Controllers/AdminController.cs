﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Donem_Odevi.Models.Data;
using System.IO;

namespace Donem_Odevi.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Admin()
        {
            if (Session["Email"] == null)
               {
                return RedirectToAction("Index", "Home");
               }
            else if (Session["Email"].ToString() != "admin@gmail.com")
                {
                    return RedirectToAction("Index", "Home");
                }
            
            return View();
        }
        public ActionResult Urunler()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var urunler = context.Urunler.ToList();
                return View(urunler);
            }
        }
        public ActionResult UrunEkle()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        
        [HttpPost]
        public ActionResult UrunEkle(Urunler s, HttpPostedFileBase file)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    Urunler _urun = new Urunler();
                    Sepet sepet = new Sepet();
                    Sepet sepetid = new Sepet();                                 
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _urun.UrunFoto = memoryStream.ToArray();
                        sepet.UrunFoto = memoryStream.ToArray();                       
                    }                    
                    _urun.UrunIsmi = s.UrunIsmi;                    
                    sepet.UrunIsmi = s.UrunIsmi;
                    _urun.Cinsiyet = s.Cinsiyet;
                    sepet.Cinsiyet = s.Cinsiyet;
                    sepet.Adet = 0;
                    _urun.UrunFiyat = s.UrunFiyat;
                    sepet.UrunFiyat = s.UrunFiyat;                    
                    context.Urunler.Add(_urun);
                    context.Sepet.Add(sepet);                   
                    context.SaveChanges();
                       
                    return RedirectToAction("Urunler", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        public ActionResult UrunSil(int UrunID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    context.Urunler.Remove(context.Urunler.First(d => d.ID == UrunID));
                    //context.Sepet.Remove(context.Sepet.First(d => d.ID == UrunID));
                    context.SaveChanges();
                    return RedirectToAction("Urunler", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        public ActionResult UrunDuzenle(int UrunID)
        {
            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _ourunDuzenle = context.Urunler.Where(x => x.ID == UrunID).FirstOrDefault();
                return View(_ourunDuzenle);
            }
        }
        [HttpPost]
        public ActionResult UrunDuzenle(Urunler b, HttpPostedFileBase file)
        {

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _urunDuzenle = context.Urunler.Where(x => x.ID == b.ID).FirstOrDefault();
                var sepet = context.Sepet.Where(x => x.SepetID == b.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _urunDuzenle.UrunFoto = memoryStream.ToArray();
                    sepet.UrunFoto = memoryStream.ToArray();
                }
                _urunDuzenle.UrunIsmi = b.UrunIsmi;
                sepet.UrunIsmi = b.UrunIsmi;
                _urunDuzenle.Cinsiyet = b.Cinsiyet;
                sepet.Cinsiyet = b.Cinsiyet;
                _urunDuzenle.UrunFiyat = b.UrunFiyat;
                sepet.UrunFiyat = b.UrunFiyat;    
                context.SaveChanges();
                return RedirectToAction("Urunler", "Admin");

            }

        }
        public ActionResult OUrunler()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var urunler = context.OnerilenUrunler.ToList();
                return View(urunler);
            }
        }
        public ActionResult OUrunlerEkle()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        [HttpPost]
        public ActionResult OUrunlerEkle(OnerilenUrunler s, HttpPostedFileBase file)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    OnerilenUrunler _urun = new OnerilenUrunler();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _urun.UrunFoto = memoryStream.ToArray();
                    }
                    _urun.UrunIsmi = s.UrunIsmi;
                    _urun.Cinsiyet = s.Cinsiyet;
                    _urun.UrunFiyat = s.UrunFiyat;
                    context.OnerilenUrunler.Add(_urun);
                    context.SaveChanges();
                    return RedirectToAction("OUrunler", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        public ActionResult OUrunSil(int UrunID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    context.OnerilenUrunler.Remove(context.OnerilenUrunler.First(d => d.ID == UrunID));
                    context.SaveChanges();
                    return RedirectToAction("OUrunler", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        public ActionResult Users()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var users = context.Users.ToList();
                return View(users);
            }
        }
        public ActionResult UsersDuzenle(int UserID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _usersDuzenle = context.Users.Where(x => x.ID == UserID).FirstOrDefault();
                return View(_usersDuzenle);
            }
        }
        [HttpPost]
        public ActionResult UsersDuzenle(Users user)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    var _usersDuzenle = context.Users.Where(x => x.ID == user.ID).FirstOrDefault();                    
                    _usersDuzenle.Name = user.Name;
                    _usersDuzenle.Email = user.Email;
                    _usersDuzenle.Password = user.Password;
                    _usersDuzenle.ConfirmPassword = user.ConfirmPassword;
                    context.SaveChanges();
                    return RedirectToAction("Users", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }
        public ActionResult UsersSil(int UsersID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    context.Users.Remove(context.Users.First(d => d.ID == UsersID));
                    context.SaveChanges();
                    return RedirectToAction("Users", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        public ActionResult Slider()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var slider = context.Slider.ToList();
                return View(slider);
            }
        }        
        public ActionResult SlideEkle()
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        public ActionResult SlideDuzenle(int SlideID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _slideDuzenle = context.Slider.Where(x => x.ID == SlideID).FirstOrDefault();
                return View(_slideDuzenle);
            }
        }
        public ActionResult SlideSil(int SlideID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    context.Slider.Remove(context.Slider.First(d => d.ID == SlideID));
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }        
        [HttpPost]
        public ActionResult SlideEkle(Slider s, HttpPostedFileBase file)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {                    
                    Slider _slide = new Slider();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slide.SliderFoto = memoryStream.ToArray();
                    }                                       
                    _slide.SliderTitle = s.SliderTitle;
                    _slide.SliderText = s.SliderText;
                    _slide.BaslangicTarih = s.BaslangicTarih;
                    _slide.BitisTarih = s.BitisTarih;
                    context.Slider.Add(_slide);
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(Slider slide, HttpPostedFileBase file)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    var _slideDuzenle = context.Slider.Where(x => x.ID == slide.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slideDuzenle.SliderFoto = memoryStream.ToArray();
                    }
                    _slideDuzenle.SliderText = slide.SliderText;
                    _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                    _slideDuzenle.BitisTarih = slide.BitisTarih;
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }
        public ActionResult OUrunlerDuzenle(int UrunID)
        {
            if (Session["Email"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (Session["Email"].ToString() != "admin@gmail.com")
            {
                return RedirectToAction("Index", "Home");
            }

            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _ourunDuzenle = context.OnerilenUrunler.Where(x => x.ID == UrunID).FirstOrDefault();
                return View(_ourunDuzenle);
            }
        }
        [HttpPost]
        public ActionResult OUrunlerDuzenle(OnerilenUrunler b, HttpPostedFileBase file)
        {
            
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    var _ourunDuzenle = context.OnerilenUrunler.Where(x => x.ID == b.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _ourunDuzenle.UrunFoto = memoryStream.ToArray();
                    }
                    _ourunDuzenle.UrunIsmi = b.UrunIsmi;
                    _ourunDuzenle.Cinsiyet = b.Cinsiyet;
                    _ourunDuzenle.UrunFiyat = b.UrunFiyat;
                    context.SaveChanges();
                    return RedirectToAction("OUrunler", "Admin");
                
            }            

        }

    }
}