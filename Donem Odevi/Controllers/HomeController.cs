﻿using Donem_Odevi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Donem_Odevi.Models.Data;
using System.Threading;
using System.Globalization;

namespace Donem_Odevi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            SepettekiElamanSayisi();
            using (DonemOdeviDb context = new DonemOdeviDb())
            {               
                AnaSayfaDTO anaSayfa = new AnaSayfaDTO();
                anaSayfa.slider = context.Slider.ToList();
                anaSayfa.urunler = context.Urunler.ToList();
                anaSayfa.onerilenurunler = context.OnerilenUrunler.ToList();                 
                return View(anaSayfa);
            }
        }        

        public ActionResult About()
        {
            SepettekiElamanSayisi();
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            SepettekiElamanSayisi();
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [ValidateAntiForgeryToken]
        public ActionResult Hesap(int UserID)
        {            
            if (Session["Email"] == null) return RedirectToAction("Index", "Home");
            SepettekiElamanSayisi();
            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                var _hesap = context.Users.Where(x => x.ID == UserID).FirstOrDefault();
                return View(_hesap);
            }
        }                
        [HttpPost]
        public ActionResult Hesap(Users user)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    var _usersDuzenle = context.Users.Where(x => x.ID == user.ID).FirstOrDefault();
                    _usersDuzenle.Name = user.Name;
                    _usersDuzenle.Email = user.Email;
                    _usersDuzenle.Password = user.Password;
                    _usersDuzenle.ConfirmPassword = user.ConfirmPassword;
                    context.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }

        public ActionResult Carts()
        {
            if (Session["Email"] == null)
            {
              return  RedirectToAction("Index", "Home");
            }            
            SepettekiElamanSayisi();
            string email = Session["Email"].ToString();      
            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                AnaSayfaDTO sepet = new AnaSayfaDTO();
                var s = context.Sepet.Where(u => u.UserEmail == email);
                if (s.Count() != 0)
                {
                    int toplam = s.Sum(u => (u.UrunFiyat) * (u.Adet));
                    ViewBag.toplam = toplam;
                }
                sepet.sepet = context.Sepet.Where(u=>u.UserEmail== email).ToList();                
                                                                 
                return View(sepet);
            }
        }
        
        public ActionResult AddCarts(int UrunID)
        {
            if (Session["Email"] == null)
            {                
                return RedirectToAction("Index", "Home");
            }
            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                Sepet sepet = context.Sepet.Single(p => p.SepetID == UrunID);                 
                sepet.UserEmail = Session["Email"].ToString();
                sepet.Adet += 1;              
                context.SaveChanges();            
                
                return RedirectToAction("Carts", "Home");
            }
        }
        
        public ActionResult RemoveCarts(int UrunID)
        {
            using (DonemOdeviDb context = new DonemOdeviDb())
            {
                Sepet sepet = context.Sepet.Single(p => p.SepetID == UrunID);                
                sepet.Adet -= 1;
                if (sepet.Adet == 0 || sepet.Adet<0)
                {
                    sepet.Adet = 1;
                }
                context.SaveChanges();
                return RedirectToAction("Carts", "Home");
            }
        }
        public ActionResult DeleteCart(int UrunID)
        {
            try
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    Sepet sepet = context.Sepet.Single(p => p.SepetID == UrunID);
                    sepet.UserEmail = null;
                    sepet.Adet = 0;
                    context.SaveChanges();
                    return RedirectToAction("Carts", "Home");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        public void SepettekiElamanSayisi()
        {
            if (Session["Email"] != null)
            {
                using (DonemOdeviDb context = new DonemOdeviDb())
                {
                    string email = Session["Email"].ToString();
                    var s = context.Sepet.Where(u => u.UserEmail == email);
                    int toplam = s.Count();                    
                    ViewBag.toplamurun = toplam;
                }
            }
        }
        public ActionResult Language(string Lang)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Lang);
            Thread.CurrentThread.CurrentUICulture =new  CultureInfo(Lang);
            HttpCookie cookie = new HttpCookie("Dil");
            cookie.Value = Lang;
            Response.Cookies.Add(cookie);
            return RedirectToAction("Index", "Home");
        }
        
    }
    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Urunler> urunler { get; set; }
        public List<OnerilenUrunler> onerilenurunler { get; set; }
        public List<Sepet> sepet { get; set; }


    }
}