﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Donem_Odevi.Startup))]
namespace Donem_Odevi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
