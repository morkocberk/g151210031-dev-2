﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Donem_Odevi.Models.Data
{
    public class Urunler
    {      

        [Key]        
        public int ID { get; set; }        
        public string UrunIsmi { get; set; }
        public byte[] UrunFoto { get; set; }
        public string Cinsiyet { get; set; }
        public int UrunFiyat { get; set; }
        //public int Adet { get; set; }
        //public int ToplamFiyat
        //{
        //    get { return Adet * UrunFiyat; }
        //}
    }
}