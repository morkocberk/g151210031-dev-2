﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Donem_Odevi.Models.Data
{
    public class DonemOdeviDb : DbContext
    {
        public DbSet<Slider> Slider { get; set; }
        public DbSet<Urunler> Urunler { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<OnerilenUrunler> OnerilenUrunler { get; set; }
        public DbSet<Sepet> Sepet { get; set; }
        
    }
}