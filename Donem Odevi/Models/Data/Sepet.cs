﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donem_Odevi.Models.Data
{
    public class Sepet
    {
        [Key]
        public int SepetID { get; set; }

        [EmailAddress]
        public string UserEmail { get; set; }
        
        public int ID { get; set; }

        public string UrunIsmi { get; set; }

        public byte[] UrunFoto { get; set; }

        public string Cinsiyet { get; set; }

        public int UrunFiyat { get; set; }

        public int Adet { get; set; }

        public int ToplamFiyat { get { return Adet * UrunFiyat; } }
        
        //[ForeignKey("ID")]
        //public Urunler UrunID { get; set; }
        

    }
}